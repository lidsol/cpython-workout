Flujo de trabajo
================

## Plan del taller -- Diego

## Presentación de LIDSOL y ponentes -- Diego
* Hola

## Comunidad -- Luis 
* Python Fundation
* Grupos de usuarios locales (eventos)
  * Búsqueda...
* Plataformas de comunicación
  * Frenode IRC
  * Listas de correos
  * Slack
  * Zulip (investigar)
* Eventos internacionales (Investigar)
  * PyCon
  * DjangoCon
  * EuroPython

## Sondeo conocimiento -- Ambos

* ¿Conocen git?
* ¿Conocen GitHub?
* ¿Sabes usar línea de comandos?
	* `cd`
	* `ls`
	* `mkdir`
	* `rm`
* ¿Trabajan?
* ¿De qué carrera son?
* ¿En qué les interesa contribuir?
* ¿De qué semestre son?

## Historia -- Luis

* Version liberada
* Version estable
* Version comparible con GPL
* Version PSF

## Leguaje Python a fondo -- Luis

* Lenguaje Hibrido

### cpython -- Diego

### jython, ironpython, pypy -- Luis

	* Diferencias
  * ¿Por qué existen otra implementaciones?
  * CPython es la implementación de referencia

>> Tiempo estimado: 40 min.

## Configuración del entorno -- Ambos
  1. Dependencias {Linux,MacOS,Windows}
  2. Cuenta de GitHub
  3. Fork al proyecto principal
  4. `$ git clone`
    * **Opcional: Curso turbo de git**
      * Conceptos elementales: Control de versiones, Ramas, Stage, Historia, Commits
      * Flujos de trabajo
      * Comandos básicos
    * Configuración de `upstream`
      * `$ git add upstream git@github.com:python/cpython.git`
    * Estructura de Directorios
			* Gramatica
			* Documentación
  5. Construcción del binario
    * `./configure  --with-pydebug`
    * `make -s -j`
    * `test`
  6. Explicación de reconstrucción cuando se hacen cambios

>> Tiempo Estimado: 1 hora.

## Modificaciones al código

0. Ejemplo: Modificación del prompt -- Luis
1. Reto: Agregar tu nombre al mensaje de bienvenida -- Diego
2. Reto: Modificar el Zen de Python -- Luis
3. Reto: Agregar una palabra reservada nueva -- Diego

>> Tiempo estimado: 40 min.

## Cazando bugs -- Ambos
* Bugs python org
  * Crear cuenta
  * Firmar acuerdo de contribución
  * Newcomer friendly
  * "Easy" issues
  * Issues with patch
* Corriendo los test
  * `./python -m test -h`
* Triage (Revisión de issues y PR)
  * Renombrando PR
  * Etiquetado de issues

### Algoritmo para cazar Bugs
  1. Encuentra un bug susceptible de ser exterminado `>> source_code.find(bug, type='easy')`
  2. Crea una rama para atacar el problema `$ git checkout -b bpo-24537`
  3. Implementa y documenta tu solución  `$ git add . && git commit "fix(core): Solucion a problema crítico"`
  4. Haz un pull request  `$ git push origin bpo-24537 && pull-request`
  5. Ajusta tu solución de acuerdo a la retroalimentación  `$ git reset -- hard`
  6. Repite desde el paso 3  `goto 3`

>> Tiempo estimado: 40 min.
