# Cómo contribuir al código de cpython
Python es un lenguaje ampliamente usado con una comunidad muy activa. Si
siempre has querido contribuir con el código fuente pero no sabes por dónde
empezar este taller es para ti. Desde la estructura del proyecto hasta tu
primera contribución

## Introducción
Te has preguntado la diferencia entre `python`, `cpython`, `jpython` y `pypy`.
En esta sección se dará una breve explicación de cuales son las diferencias
entre estos conceptos. Por otro lado, explicaremos quién compone la comunidad
de python, dónde encontrarla y como introducirse en el proyecto en general así
como un vistazo general a la guía de desarrolladores 

## Preparando el entorno de desarrollo
Apoyaremos en la obtención del código fuente de cpython, instalación de
dependencias, compilación y puesta en marcha del interprete en su versión alfa. 

## Otras formas de contribución
No soy programadxr pero quiero ser parte de este proyecto ¿Cómo puedo
contribuir? En está sección daremos un breve recuento de formas alternativas de
contribución a python 

## Into the cpython project
Veremos la estructura del proyecto y después de una serie de retos (con sus
premios correspondientes) estaremos listos para agregar nuestro granito de
arena al enorme proyecto.

## Bug hunters
¡A cazar bugs!

### NOTA: Conocimientos esperados del público
* Manejo de git y github
* Manejo básico de terminal
* Ganas de contribuir

#### Requerimientos técnicos

* Proyector
* HDMI
* Internet para los ponentes y asistentes

#### Recursos y referencias
* [Workshop cpython CCOSS](https://lifesconundrum.me/pycontrib-01-intro.html)
* [Python Dev Guide](devguide.python.org)
* [Git Bootcamp](devguide.python.org/gitbootcamp)
* [Issue Tracker](bugs.python.org)
* [Documentación python en Español](https://github.com/raulcd/python-docs.es/)
* [cpython internal docs](https://cpython-core-tutorial.readthedocs.io/en/latest/internals.html)
